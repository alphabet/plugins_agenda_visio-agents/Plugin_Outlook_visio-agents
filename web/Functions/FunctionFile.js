// -----------------------------------------------------------------
const clientId = 'BBB-Visio';
const authBaseUrl =
  'https://authent.webinaire.numerique.gouv.fr/auth/realms/master';
const relativeAuthorizeUrl = '/protocol/openid-connect/auth';
const apiUrl = 'https://visio-test.education.fr/api/meetings';
const meetingsManagerUrl = 'https://visio-test.education.fr/welcome';

const defaultInvitationTitle = 'Séminaire BBB';
const defaultInvitationModeratorLink =
  "Cliquez ici pour rejoindre la salle '%name%' (Lien modérateur)";
const defaultInvitationParticipantLink =
  "Cliquez ici pour rejoindre la salle '%name%'";

const invitationHtmlTemplate =
  "<br/><br/><hr/><h3>%invitationTitle%</h3><a href='%url%'>%invitationLink%</a>";

const listItemTitle = '%name%';
const listItemSubtitle = "Choisissez le lien à ajouter à l'invitation";
const listItemModeratorLinkText = 'Lien modérateur';
const listItemModeratorLinkTooltip =
  "Ajouter le lien modérateur à l'invitation";
const listItemParticipantLinkText = 'Lien participant';
const listItemParticipantLinkTooltip =
  "Ajouter le lien participant à l'invitation";
const listItemMeetingLinkTooltip = 'Accèder à la salle';

// -----------------------------------------------------------------
Office.initialize = function () {};

// -----------------------------------------------------------------
function GotoMeetingsManager() {
  window.open(meetingsManagerUrl);
}

// -----------------------------------------------------------------
function GotoMeeting(meetingUrl) {
  window.open(meetingUrl);
}

// -----------------------------------------------------------------
function AddLocationToInvitation(url, name, isModerator) {
  const settings = Office.context.roamingSettings;

  let invitationTitle = settings.get('invitationTitle');
  let invitationModeratorLink = settings.get('invitationModeratorLink');
  let invitationParticipantLink = settings.get('invitationParticipantLink');

  if (Office.context.mailbox.item.location) {
    Office.context.mailbox.item.location.getAsync((getResult) => {
      if (getResult.status == Office.AsyncResultStatus.Failed) {
        console.log(`Action failed with message ${typeResult.error.message}`);
      } else {
        var location = getResult.value;

        if (location) {
          location += ';' + url;
        } else {
          location = url;
        }

        Office.context.mailbox.item.location.setAsync(location, (result) => {
          if (result.status !== Office.AsyncResultStatus.Succeeded) {
            console.log(`Action failed with message ${result.error.message}`);
            return;
          }
          console.log(`Successfully set location to ${url}`);
        });
      }
    });
  }

  if (Office.context.mailbox.item.body) {
    let htmlBodyUrl = invitationHtmlTemplate
      .replace('%invitationTitle%', invitationTitle.replace('%name%', name))
      .replace('%url%', url)
      .replace(
        '%invitationLink%',
        (isModerator
          ? invitationModeratorLink
          : invitationParticipantLink
        ).replace('%name%', name)
      );

    Office.context.mailbox.item.body.getAsync(
      Office.CoercionType.Html,
      function (getResult) {
        if (getResult.status == Office.AsyncResultStatus.Failed) {
          console.log(`Action failed with message ${typeResult.error.message}`);
        } else {
          var body = getResult.value;

          body += htmlBodyUrl;

          Office.context.mailbox.item.body.setAsync(
            body,
            { coercionType: Office.CoercionType.Html },
            (result) => {
              if (result.status !== Office.AsyncResultStatus.Succeeded) {
                console.log(
                  `Action failed with message ${result.error.message}`
                );
                return;
              }
              console.log(`Successfully set body`);
            }
          );
        }
      }
    );
  }
}

// -----------------------------------------------------------------
function GenerateRandomNonce(length) {
  var text = '';
  var possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (var i = 0; i < length; i++) {
    text += possible.charAt(
      Math.floor(window.crypto.getRandomValues() * possible.length)
    );
  }
  return text;
}

// -----------------------------------------------------------------
function SaveSettings() {
  const settings = Office.context.roamingSettings;

  const invitationTitle = $('#txtInvitationTitle').val();
  const invitationModeratorLink = $('#txtInvitationModeratorLink').val();
  const invitationParticipantLink = $('#txtInvitationParticipantLink').val();

  settings.set('invitationTitle', invitationTitle);
  settings.set('invitationModeratorLink', invitationModeratorLink);
  settings.set('invitationParticipantLink', invitationParticipantLink);

  settings.saveAsync(function (asyncResult) {
    console.log(asyncResult);
  });
}

// -----------------------------------------------------------------
function loadSettings() {
  const settings = Office.context.roamingSettings;

  let invitationTitle = settings.get('invitationTitle');
  let invitationModeratorLink = settings.get('invitationModeratorLink');
  let invitationParticipantLink = settings.get('invitationParticipantLink');

  if (!invitationTitle) {
    invitationTitle = defaultInvitationTitle;
  }

  if (!invitationModeratorLink) {
    invitationModeratorLink = defaultInvitationModeratorLink;
  }

  if (!invitationParticipantLink) {
    invitationParticipantLink = defaultInvitationParticipantLink;
  }

  $('#txtInvitationTitle').val(invitationTitle);
  $('#txtInvitationModeratorLink').val(invitationModeratorLink);
  $('#txtInvitationParticipantLink').val(invitationParticipantLink);
}

// -----------------------------------------------------------------
function htmlEncode(input) {
  return input
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
}

function getMeetings() {
  if (OfficeHelpers.Authenticator.isAuthDialog()) return;

  $('#meetings-list').html('');

  $('#spinner').show();
  $('#manageButton').hide();
  $('#refreshButton').hide();

  const authenticator = new OfficeHelpers.Authenticator(
    new OfficeHelpers.EndpointStorage(
      OfficeHelpers.StorageType.InMemoryStorage
    ),
    new OfficeHelpers.TokenStorage(OfficeHelpers.StorageType.InMemoryStorage)
  );

  authenticator.endpoints.add('OpenIdConnectProvider', {
    clientId: clientId,
    baseUrl: authBaseUrl,
    authorizeUrl: relativeAuthorizeUrl,
    redirectUrl:
      location.href.replace(/\/[^\/]+?\.[^\/]+?$/, '/') +
      'AuthPopup_callback.html',
    scope: 'openid email profile',
    state: GenerateRandomNonce(10),
    nonce: GenerateRandomNonce(10),
    responseType: 'id_token token',
  });

  authenticator
    .authenticate('OpenIdConnectProvider', true)
    .then(function (token) {
      $.ajax({
        url: apiUrl,
        method: 'GET',
        beforeSend: function (request) {
          request.setRequestHeader('Authorization', 'Bearer ' + token.id_token);
        },
      })
        .done(function (data, textStatus, jqXHR) {
          if (data && data.meetings && data.meetings.length > 0) {
            data.meetings.forEach((meeting) => {
              $('#meetings-list').append(
                `<li class="ms-ListItem is-unread apitech-shadow-ListItem" tabindex="0">
                        <span class="ms-ListItem-primaryText">
                            ${listItemTitle.replace(
                              '%name%',
                              htmlEncode(meeting.name)
                            )}&nbsp;
                            <a class="ms-Link" title="${listItemMeetingLinkTooltip}" href="#" onClick="GotoMeeting('${htmlEncode(
                  meeting.moderator_url
                )}');">
                                <i class="ms-Icon ms-Icon--Link"></i>
                            </a>
                        </span>
                        <span class="ms-ListItem-tertiaryText">${listItemSubtitle.replace(
                          '%name%',
                          htmlEncode(meeting.name)
                        )}</span>
                        <div class="ms-Grid" dir="ltr">
                            <div class= "ms-Grid-row">
                                <a class="ms-Link ms-Grid-col ms-sm6 ms-md6 ms-lg6" href="#" onClick="AddLocationToInvitation('${htmlEncode(
                                  meeting.moderator_url
                                )}', '${htmlEncode(
                  meeting.name
                )}', true);" title="${listItemModeratorLinkTooltip}">
                                    <i class="ms-Icon ms-Icon--PresenceChickletVideo"></i>
                                    &nbsp;${listItemModeratorLinkText}
                                </a>
                                <a class="ms-Link ms-Grid-col ms-sm6 ms-md6 ms-lg6" href="#" onClick="AddLocationToInvitation('${htmlEncode(
                                  meeting.attendee_url
                                )}', '${htmlEncode(
                  meeting.name
                )}', false);" title="${listItemParticipantLinkTooltip}">
                                    <i class="ms-Icon ms-Icon--Video"></i>
                                    &nbsp;${listItemParticipantLinkText}
                                </a>
                            </div>
                        </div>
                    </li>`
              );
            });
          }
        })
        .fail(function (data, textStatus, jqXHR) {
          console.log(
            'error : ' +
              textStatus +
              ':' +
              jqXHR.status +
              ':' +
              jqXHR.responseURL
          );
        })
        .always(function (data, textStatus, jqXHR) {
          $('#spinner').hide();
          $('#manageButton').show();
          $('#refreshButton').show();
        });
    })
    .catch(function (err) {
      console.warn(err);

      $('#spinner').hide();
      $('#manageButton').show();
      $('#refreshButton').show();
    });

  // Mode bouchon
  //result_meetings = {
  //    "meetings": [
  //        {
  //            "attendee_url": "https://visio-test.education.fr//meeting/signin/252/creator/110/hash/6258d8a5af886724cb037ea1f0751871a6fbd281",
  //            "moderator_url": "https://visio-test.education.fr//meeting/signin/252/creator/110/hash/1aa6c856fabd45627064c2f3c4ac19c3b7006c77",
  //            "name": "Mon S\u00e9minaire"
  //        },
  //        {
  //            "attendee_url": "https://visio-test.education.fr//meeting/signin/252/creator/110/hash/6258d8a5af886724cb037ea1f0751871a6fbd281",
  //            "moderator_url": "https://visio-test.education.fr//meeting/signin/252/creator/110/hash/1aa6c856fabd45627064c2f3c4ac19c3b7006c77",
  //            "name": "Mon S\u00e9minaire 2"
  //        },
  //        {
  //            "attendee_url": "https://visio-test.education.fr//meeting/signin/252/creator/110/hash/6258d8a5af886724cb037ea1f0751871a6fbd281",
  //            "moderator_url": "https://visio-test.education.fr//meeting/signin/252/creator/110/hash/1aa6c856fabd45627064c2f3c4ac19c3b7006c77",
  //            "name": "Mon S\u00e9minaire 3"
  //        }
  //    ]
  //};
}
