﻿(function () {
    "use strict";

    var _settings;

    // La fonction d'initialisation Office doit être exécutée chaque fois qu'une nouvelle page est chargée.
    Office.initialize = function (reason) {
        $(document).ready(function () {

            getMeetings();
        });
    };
})();