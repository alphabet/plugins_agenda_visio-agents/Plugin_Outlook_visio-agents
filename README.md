# Usage
/!\ Ce plugin fonctionne avec le frontal B3Desk : https://github.com/numerique-gouv/b3desk

/!\ Ce plugin ne peut être chargé dans Outlook que via un compte Exchange

(informations disponibles dans /documentation)
1. Héberger /web sur un serveur web
2. mettre à jour le fichier manifest situé dans /manifest avec cette information
3. Installer le fichier Manifest BBBOutlookWebAddIn_visio-tests.xml sur le client outlook
